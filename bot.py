import sys
import pprint
import signal
import time
import virwox
import argparse
import threading
import logging
import secrets as s

#import httplib as http_client
#http_client.HTTPConnection.debuglevel = 1
log = logging.getLogger(__name__)

class Bot(threading.Thread):
    def __init__(self, username, password, apikey):
        threading.Thread.__init__(self)
        self.client = virwox.Virwox(username, password, apikey)
        self.buys = []
        self.sells = []
        self.alive = True
        log.info('Starting...')

    def gain(self,buy,sell,amount,comm):
        return round((sell-buy)*amount - (sell+buy)*amount*comm,2)

    def run(self):
        count = 0
        operation = 0
        gain = 0

        # abrimos log
        f = open(s.file_status,"w")
        
        # traemos el balance
        self.client.balance()
        # guardamos el balance original
        d = str(self.client)

        while self.alive:
            # TODO si el precio se corrio cancelamos ventas y compras
            # hay un caso particular otro bot que pone uno mas de buy
            # si ponemos 15500 mas por lo general el bot no lo sigue
            # igualmente hay que guardar la venta original para ver si 
            # hay ganancia

            # traemos los mejores precios
            self.client.bestprices()
            buy = self.client.bestbuy
            sell = self.client.bestsell

            # TODO la comision cambia
            # buscamos la ganancia neta
            gain = self.gain(buy,sell,s.amount_btc,s.commission)

            # chequeamos si hay ordenes
            if count % 30 == 0:
                orders = self.client.orders()
                log.info(str(self.client) + " gain: " + str(gain))
                log.info(self.client.info(s.commission))

            # tomamos el unixtime
            mytime = int(time.time())

            # la ganancia es mayor amount_gain y no hay ordenes pendientes
            if gain > s.amount_gain and orders is None:

                # compramos btc en mejor precio
                order_buy = {
                    'id' : self.client.buy(s.amount_btc,buy),
                    'time' : mytime
                }

                # vendemos btc al mejor precio
                order_sell = {
                    'id' : self.client.sell(s.amount_btc,sell),
                    'time' : mytime
                }

                # TODO debemos re intentar un par de veces
                # cancelamos si hubo error
                if order_buy['id'] > 0 and order_sell['id'] == 0:
                    log.info('cancel buy %s' % self.client.cancel(order_buy['id']))

                if order_sell['id'] > 0 and order_buy['id'] == 0:
                    log.info('cancel sell %s' % self.client.cancel(order_sell['id']))

                # calculamos balance
                self.client.balance()

                # recargamos ordenes
                orders = self.client.orders()

            # calculamos balance
            if count % 300 == 0:
                self.client.balance()
                txt = time.asctime()+ "\n\n"+ d+ "\n\n"+ str(self.client)+ "\n"
                txt += "gain="+str(gain)+ "\n\n"
                txt += self.client.info(s.commission)+ "\n\n"
                txt += pprint.pformat(orders)
                f.truncate()
                f.seek(0)
                f.write(txt)
                f.flush()
                count = 0

            # dormimos
            time.sleep(s.sleep_time)

            count += 1


def handler(signal, frame):
    print 'Exiting...'
    b.alive = False
    sys.exit(0)

if __name__ == '__main__':
    #f = logging.Formatter('%(asctime)s|%(levelname)s|%(message)s')
    #l = logging.StreamHandler(sys.stdout)
    #l.setLevel(logging.DEBUG)
    #l.setFormatter(f)
    #logging.getLogger().addHandler(l)

    parser = argparse.ArgumentParser()
    parser.add_argument('-d','--debug',action='store_true',default=False)
    args = parser.parse_args()
    
    if args.debug:
        level=logging.DEBUG
    else:
        level=logging.INFO

    f = '%(asctime)s|%(levelname)s|%(message)s'
    logging.basicConfig(level=level,format=f)
    logging.getLogger('requests').setLevel(logging.WARNING)
    logging.getLogger('urllib3').setLevel(logging.WARNING)

    signal.signal(signal.SIGINT, handler)
    signal.signal(signal.SIGTERM, handler)
    b = Bot(s.username, s.password, s.key)
    b.daemon=True
    b.start()
    signal.pause()


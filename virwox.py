import requests
import logging

log = logging.getLogger(__name__)

class Virwox(object):
    def __init__(self, username, password, key):
        self._user = username
        self._pass = password
        self._key = key
        self._usd = 0
        self._sll = 0
        self._btc = 0
        self._bestsell = 0
        self._bestbuy = 0
        self._bestsellusd = 0
        self._bestbuyusd = 0

    def __str__(self):
        return "balance: usd=%0.2f sll=%0.2f btc=%0.2f\nprices sll: sell=%d buy=%d diff=%d\nprices usd: sellusd=%0.2f buyusd=%0.2f diffusd=%0.2f" % (
            self._usd,
            self._sll,
            self._btc,
            self._bestsell, 
            self._bestbuy, 
            self.diff,
            self._bestsellusd,
            self._bestbuyusd,
            self.diffusd
            )

    @property
    def bestsell(self):
        return self._bestsell

    @property
    def bestbuy(self):
        return self._bestbuy

    @property
    def diff(self):
        return self._bestsell - self._bestbuy

    @property
    def diffusd(self):
        return self._bestsellusd - self._bestbuyusd

    def info(self,comm):
        return "btc: buy %0.2f sell %0.2f" % (self._bestbuy / self._bestsellusd * (1+comm), self._bestsell / self._bestbuyusd * (1+comm))

    def bestprices(self):
        try: 
            r = requests.post(
                'http://api.virwox.com/api/json.php',
                json={
                    "method":"getBestPrices",
                    "params": { "symbols": ["BTC/SLL","USD/SLL"] }
                }
            )

            log.debug('bestprices %s' % r.json())
            self._bestbuy = int(r.json()['result'][0]['bestBuyPrice'])
            self._bestsell = int(r.json()['result'][0]['bestSellPrice'])
            self._bestbuyusd = float(r.json()['result'][1]['bestBuyPrice'])
            self._bestsellusd = float(r.json()['result'][1]['bestSellPrice'])
        except Exception as e:
            log.error('ERROR getBestPrices json', str(e))

    def sell(self,amount,price):
        log.info('sell %s %s' % (amount, price))
        try: 
            r = requests.post(
                'http://api.virwox.com/api/trading.php',
                json={
                    "method":"placeOrder",
                    "params": { 
                        "key": self._key,
                        "user": self._user,
                        "pass": self._pass,
                        "instrument": "BTC/SLL",
                        "orderType": "SELL",
                        "amount": amount,
                        "price": price
                    }
                }
            )

            log.debug('sell %s' % r.json())
            return r.json()['result']['orderID']
        except Exception as e:
            log.error('ERROR placeOrder sell', str(e))
            return 0

    def buy(self,amount,price):
        log.debug('buy %s %s' % (amount, price))
        try: 
            r = requests.post(
                'http://api.virwox.com/api/trading.php',
                json={
                    "method":"placeOrder",
                    "params": { 
                        "key": self._key,
                        "user": self._user,
                        "pass": self._pass,
                        "instrument": "BTC/SLL",
                        "orderType": "BUY",
                        "amount": amount,
                        "price": price
                    }
                }
            )

            log.debug('buy %s' % r.json())
            return r.json()['result']['orderID']
        except Exception as e:
            log.error('ERROR placeOrder buy', str(e))
            return 0

    def balance(self):
        try:
            r = requests.post(
                'http://api.virwox.com/api/trading.php',
                json={
                    "method":"getBalances",
                    "params": { 
                        "key": self._key,
                        "user": self._user,
                        "pass": self._pass,
                    }
                }
            )

            log.debug('balance %s' % r.json())
            self._usd = r.json()['result']['accountList'][0]['balance']
            self._sll = r.json()['result']['accountList'][1]['balance']
            self._btc = r.json()['result']['accountList'][2]['balance']
        except Exception as e:
            log.error('ERROR balance', str(e))

    def orders(self):
        try: 
            r = requests.post(
                'http://api.virwox.com/api/trading.php',
                json={
                    "method":"getOrders",
                    "params": { 
                        "key": self._key,
                        "user": self._user,
                        "pass": self._pass,
                        "selection": "OPEN",
                    }
                }
            )

            log.debug('orders %s' % r.json())
            return r.json()['result']['orders']
        except Exception as e:
            log.error('ERROR getOrders', str(e))
            return 'ERROR_ORDERS'

    def cancel(self,orderid):
        try:
            r = requests.post(
                'http://api.virwox.com/api/trading.php',
                json={
                    "method":"cancelOrder",
                    "params": { 
                        "key": self._key,
                        "user": self._user,
                        "pass": self._pass,
                        "orderID": orderid
                    }
                }
            )

            log.debug('cancel %s' % r.json())
            return r.json()['result']['errorCode']
        except Exception as e:
            log.error('ERROR cancel', str(e))
            return 'ERROR_CANCEL'


Virwox Bot
==========

Robot simple para usar en Virwox


Funcionamiento
--------------

Genera una compra y una venta al mismo tiempo si no hay ordenes previas y si la
ganancia es la buscada. Esperando ganar con dicha diferencia.

Carga dos ordenes:

- Comprar en la mejor cola de compra "buy" 
- Vender en la mejor cola de venta "sell"

Siempre y cuando exista una ganancia "gain" que se calcula deduciendo las 
comisiones del mercado.

El bot genera un archivo de file_status con la siguiente informacion

```
Fri Jun 16 20:29:26 2017                                         <- fecha estado
usd=0.67 sll=13592.79 btc=0.07 sell=0 buy=0 diff=0             <- estado inicial
usd=0.67 sll=13908.03 btc=0.06 sell=644827 buy=616005 diff=28822       <- estado
gain=-6.81                             <- medicion la ganancia usando sell y buy
[{u'amountFilled': u'0.000000000',                         <- ordenes pendientes
  u'amountOpen': u'0.010000000',
  u'cancelledAt': None,
  u'commission': u'0.00',
  u'discountPct': u'40',
  u'filledAt': None,
  u'instrument': u'BTC/SLL',
  u'orderID': u'68851271',
  u'orderStatus': u'OPEN',
  u'orderType': u'BUY',
  u'placedAt': u'2017-06-16 18:14:30',
  u'price': u'570001',
  u'volumeFilled': u'0.00'}]
```

Cuando se imprime el objeto Virwox se detalla:

```
usd=0.67 sll=13908.03 btc=0.06 sell=644827 buy=616005 diff=28822
 ^        ^            ^        ^           ^          ^
 |        |            |        |           |          |
 |        |            |        |           |           `- diferencia sell y buy
 |        |            |        |            `- mejor valor de compra (buy)
 |        |            |         `- mejor valor de venta (sell)
 |        |             `- saldo btc 
 |         `- saldo sll
  `- saldo usd
```

El archivo virwox.py define la clase Virwox que permite realizar las operaciones
en el market Virwox.


Configuracion
-------------

Se debe editar secrets.py con la configuracion propia de credenciales y api.

```
mv secrets_sample.py secrets.py
```

Tambien es posible cambiar el monto a comprar y la ganancia esperada en sll.

```
amount_btc = 0.01  <- cantidad de btc a comprar y vender
amount_gain = 50   <- ganancia que se busca minimanente en la operacion
```

Para arrancar el bot

```
python bot.py
```

TODO
----

- se espera a vaciar las ordenes, pero se podrian tener varias en simultaneo
- guardar en una base los acumulados y todas las operaciones
- armar una interface web de reporte
 
